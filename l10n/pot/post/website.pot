# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-07-06 18:11+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: ./content/fr/post/website.adoc:1
#, no-wrap
msgid "---\n"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:8
#, no-wrap
msgid ""
"title:  Mise-à-jour du site web !\n"
"date:  2020-07-06\n"
"type: post\n"
"image: /images/website.png\n"
"categories: [\"technology\"]\n"
"tags: [\"tech\", \"hugo\", \"css\" ]\n"
"---\n"
msgstr ""

#. type: Title =
#: ./content/fr/post/website.adoc:10
#, no-wrap
msgid "Mise-à-jour du site web !"
msgstr ""

#. type: Target for macro image
#: ./content/fr/post/website.adoc:22
#, no-wrap
msgid "website.png"
msgstr ""

#. type: Title ==
#: ./content/fr/post/website.adoc:24
#, no-wrap
msgid "Ah en tout cas, il y a la continuité dans le changement ! "
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:29
#, no-wrap
msgid ""
"J'ai viré mon site web du platforme link:https://www.antora.org[antora], "
"vers une solution Hugo.  \n"
"Ces deux technologies sont des GSS (Générateurs de site statique).   \n"
"Par contre l'excellent 'Antora' était plutôt une expérience pour "
"'expérimenter' sa fonctionalité.  \n"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:32
#, no-wrap
msgid ""
"Antora c'est la solution par excelence pour gérer la documentation.  Je "
"savais parfaitment que la solution ne s'adapte pas façilement à un blog. \n"
"Son seule point faible pour l'instant c'est le manque de fonctionalité pour "
"les sites et documentation multilingue.   Je suis convaincu que ses auteurs "
"vont rémédier à cela, pusique nous vivons dans une monde multilingue!\n"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:34
msgid "Cela dit, en basculant vers http://hugo.org[Hugo], j'avais trois objectifs:"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:36
msgid "Site multilingue"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:37
msgid "Utiliser un CSS 'framework'"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:38
msgid "Gérer documents Asciidoctor"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:40
msgid ""
"Quand à créer un site multilingue avec générateur de site statique à mon "
"avis il n'y pas mieux que link:http://gohugo.io[hugo].  Sans aller dans tous "
"les détails c'est l'outil le plus rapide, très bien documenté, et gérent de "
"façon exceptionelle de multiples langues."
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:47
#, no-wrap
msgid ""
"Pour le 2ie point, CSS framework, j'ai selectioné Bulma.   \n"
"J'ai fait assez de recherches et j'aurai pu en chosir d'autres.  \n"
"Bulma n'est pas 'gros', est basé sur flexbox, n'a pas de java, (avangageux), "
"et a une large communauté d'utilisaeurs. \n"
"Par contre j'a un point de vue équilibré sur les 'framework' CSS.   \n"
"Dans mon nouveau site par example j'ai utilisé des élémments 'grid' au lieux "
"de flexbox pour lister des posts.  \n"
"À mon avis, bien que le dévélopment web n'est pas mon métier, que pour celui "
"qui poursuit cela, mieux vaut faire son propre échafaudage du site avec "
"grid/flexbox, et ensuite utiliser le css frameworks pour les boutons, tags "
"et autres éléments.   De cette façon on pourrait faire un site une peu plus "
"'portable'.  Et encore plus important, la connaissance aquise serait aussi "
"plus 'portable', et peut s'utiliser partout.\n"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:49
msgid ""
"Mon site utiliser exclusivement les documents en format Asciidoctor.  C'est "
"un format qui permet de convertir *facilement* vers docbook, (son origine), "
"html, pdf, et cela en produisant de très joli documents.  Je ne peux pas "
"dire que l'intégration avec Hugo est facile. Hugo doit faire apelle a "
"l'utilitaire externe asciidoctor pour convertir les pages.  Et par contre la "
"langue de programmation 'Go' dans laquelle Hugo est écrite gére la "
"conversion.  Éspérons que dans l'avenir Hugo va gagner plus de souplesse "
"dans ce domaine."
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:51
#, no-wrap
msgid ""
"Note:Mais un autre défit qu'il faut mentionner ici c'est qu'il y a forcément "
"un mélange de styles CSS de Bulma et Asciidoctor.  Pour l'instant j'ai du "
"faire peu d'ajustments manuelles pour corriger les 'conflits de personalité' "
"de Bulma et Asciidoctor CSS.   Sans doute avec le temps d'autres vont "
"apparaître.  Mais c'était au moins en parti possible de laisser Bulma "
"formatter les boutons les 'cartes' et autes éléments, et imposer les styles "
"Asciidoctor sur les documents proprement dit.  Je suis content du "
"résultat.\n"
msgstr ""

#. type: Plain text
#: ./content/fr/post/website.adoc:52
#, no-wrap
msgid ""
"Alors voila, il y a des ajustments à faire.   Après quelque temps, je vais "
"soumettre le 'thème' au bibliothèque de Hugo.\n"
msgstr ""
