---
title:  Yet Still Again ?
date:  2018-05-21 22:46:26 +0000
type: post
tags: ["langue", "français"]
categories: ["Anglais"]
---

= Yet Still Again ?
:author: Boyd Kelly
:date: 2018-05-21 22:46:26 +0000
:description: Yet? Still? Again?
:type: post
:lang: fr 

include::./locale/attributes.adoc[fr]

== Quelle est la différence entre ces trois termes ?

Souvent le français est plus précis que l'anglais... surtout en matière de verbes.  Par contre l'anglais peut aussi être plus précis que le français.

[glossary]
== Voici un example

[glossary]
Yet:: Encore
Again:: Encore 
Still:: Encore

et aussi:

Even:: Encore

Non seulement 4 mots en anglais, mais 4 significations différentes avec la même traduction en français.  Connaissez-vous la nuance?

Voici des phrases pour vous aider:

She hasn't come yet.  =  Elle n'est pas encore venue
She is eating again.  = Elle mange encore.
She is still sleeping.  =  Elle dort encore.

It's even more difficult than I thought.  = C'est encore plus difficile que je pensais.
