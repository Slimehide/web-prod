---
title:  Tapez en Baoulé ou Julakan comme un boss sous Android !
date:  2020-04-23T20:32:08Z
type: post
image: /images/clavier_android.png
---

= Tapez en Baoulé ou Julakan comme un boss sous Android !
Author Boyd Kelly
:imagesdir: /images/
:lang: fr
:date: 2020-04-23T20:32:08Z
:type: post
:description: Tapez en Baoulé ou Julakan
:tags: julakan
:topics: technology, language
:icons: image
include::./locale/attributes.adoc[fr]

image::clavier_android.png[]

== « I ni sɔgɔma! Koow be di?  Bara ka ɲi wa? »

=== Pouvez-vous écrire ça sur votre clavier de portable ?  Si !  

=== A ka nɔgɔ!

Maintenant le clavier GBoard de Google est disponible en plus de 500 langues!
Et bien qu'on l'attend toujours en Senoufo, Guere, et Yacouba entre autres, déjà les claviers Baoulé et Jula sont disponibles.
Puisque ces claviers sont installés au niveau du système, ils sont disponibles dans toute les applications.
Alors que vous allez envoyer un mail, taper un message sur les réseaux sociaux ou bien écrire une lettre vous n'aurez pas de problème!  

[NOTE]
====
Vous devez avoir le clavier GBoard installé sur votre appareil.
Par contre, Samsung par exemple utilise son propre clavier (moins performant) par défaut.
Il se peut que vous devez aller sur Google Play, chercher gboard, puis installez ce clavier, et l'activer par défaut.
====

=== Si vous utilisez déjà Gboard, ou bien vous venez de l'installer, maintenant il s'agit d'activer la langue que vous voulez.

[cols="2"]
|===
a|image::Screenshot_20200424-191237.png[keyboard,400,,float="left",align="center"]
a|
* Allez d'abord aux Paramètres, Système, Langues et saisi.
|===

[cols="2"]
|===
a|
* Ensuite notez bien la partie *'Clavier et modes de saisie'*
* Tapez sur l'option *'Clavier virtuel'*.   
* Ensuite taper sur *'Langues'*.
a|image::Screenshot_20200424-191500.png[keyboard,400]
|===

[cols="2"]
|===
a|image::Screenshot_20200424-191614.png[keyboard,400]
a|* Tapez sur *'Ajouter un clavier'*

 * Trouvez *Dioula* ou bien *Baoulé*
|===

. A partir de ces options vous pouvez aussi choisir une configuration azerty, qwerty, et dans le cas de Jula, il y même l'option Nko!

. Si maintenant vous faites une longue presse sur la barre d'espace, vous aurez l'option de changer de clavier.

[TIP]
====
Vous pouvez taper sans problème le français sur le clavier Julakan ou Baoulé.  Par contre le correcteur d'orthographe est lié au clavier choisi.  Donc vous trouverez sans doute avantage de basculer entre les claviers.  Mais une touche longue et c'est fait.
====

== Ayiwa.  An ka taa!

