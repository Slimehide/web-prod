---
title: La Bible 
date:  2020-05-08T14:07:30
type: post
draft: true
tags: [ "jula", "language"]
categories: ["Julakan" ]
hyperlink: "https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US"
---

:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: bible.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: fr 
include::locale/attributes.adoc[fr]

= La Bible
Comparer des textes traduits en Jula de Côte d'Ivoire avec la version Louis Ségond.

link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US[Bible en Jula avec audio et français sur Google Play]

