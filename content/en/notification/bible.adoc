---
title: La Bible
date:  2020-05-08T14:07:30
type: notification
draft: true
tags: [ "jula", "language"]
categories: ["Julakan" ]
hyperlink: "https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US"
image: /images/dyu_bible.
---

:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: bible.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula ]
:lang: en

= La Bible
Compare the text of the Bible in Ivory Coast Jula with the French Louis Ségon verions.

link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible&hl=en_US[Bible en Jula avec audio et français sur Google Play]
