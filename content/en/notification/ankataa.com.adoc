---
title: Ankataa
date:  2020-05-08T14:07:30
type: notification 
tags: [ "jula", "language"]
categories: ["Julakan" ]
image: /images/ankataa.png 
hyperlink: https://www.ankataa.com
---

= Le site web https://www.ankataa.com[Ankataa]
:author: Boyd Kelly
:email:
:date: 2020-05-08T14:07:30
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: en 

An expert linguist providing online courses, in depth research as well an awesome Youtube channel for learning basic Jula and Bambara. 
Great for Jula learniers in Ivory Coast. (Just substitute 'ka' for ye, and 'le' or 'lo' for 'don'...  and a few others but after all this *is* mandenkan!)


[width="80%",cols="2",frame="topbot",options=stripe="even"]
|====
|Web site:|link:https://www.ankataa.com[Ankataa]
|Youtube channel:|link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]
|====

