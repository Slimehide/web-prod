---
title:  a sen b'a la
date:  2018-08-07T07:05:01+00:00
type: 
category: ["Julakan"]
tags: ["jula language"]
---

= a sen b'a la
:author: Boyd Kelly
:date: 2018-08-07T07:05:01+00:00
:description: A sen b'a la
:lang: en

== He is involved.

[cols=",",]
|===
|*a sen b’a la*
|He is involved / He's a complice.
(His foot is in it.)
|===
