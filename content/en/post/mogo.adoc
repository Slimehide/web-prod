---
title:  Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye
date:  2018-05-26T20:32:08Z
type: post
tags: ["jula", "proverb", "language"]
categories: ["Julakan"]
---

= Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye
:author: Proverb 
:date: 2018-05-26T20:32:08Z
:description: Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye
:draft: false
:lang: en
:type: post
:skip-front-matter:


|===
| Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye.

| C'est l'homme qui fait l'homme.
| Comme le fer aiguise le fer, ainsi un homme aide son ami à progresser -Prov 27:17 *_TMN_*
| As iron Sharpens iron, So one man sharpens his friend. -Prov 27:17 *_NWT_*
|===
