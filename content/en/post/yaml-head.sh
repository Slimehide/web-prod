#!/usr/bin/bash
[[ -z $1 ]] && { echo "need a file" ; exit 1 ; }
#sed -n '/^---\s/q0' $1 || { echo "this looks like it already has a header" ; exit 1 ; }
#this returns 0 when its NOT found.   
sed  -r -n '/^---(\s|$)/q1' $1 || { echo " This looks like it already has a header" ; exit 1 ;  }

title=`sed -n /^\=\ .*/p $1 | sed 's/=//g'`
date=`sed -n /^:date:.*/p $1 | sed 's/^:date://g'`

sed -i '1 i ---' $1
sed -i "1 a title: $title" $1
sed -i "2 a date: $date" $1
sed -i '3 a type: post' $1
sed -i '4 a ---' $1
#sed -r -i '5 a \n' $1
#this is the weirdest hack
#why can't i insert a blank line with sed?
sed -i '5 a #;' $1 && sed -i 's/^#;//g' $1
