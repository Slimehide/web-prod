---
title: No place like home!
date:  2020-05-05T12:11:55
type: home
author: Boyd Kelly
---

= No place like home!
:author: Boyd Kelly
:email:  
:date: 2020-05-05T12:11:55
:filename: index.adoc
:imagesdir: /images/
:draft: false
:keywords: ["index", "home", "technologie, technology, jula, julakan, dioula, dyula, abidjan, "côte d'ivoire"]
:tags: ["tech", "android", "afrique"]
:topics: ["Technology"]
:lang: en

image::tai.jpg[]

== Language and Technology in Africa
